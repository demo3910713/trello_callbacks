function allCardsThatBelongToaParticularListBasedOnThelistID( data, listID, cb) {
  setTimeout(() => {
    let card ;
    for (const key in data) {
        if (key === listID) {
            card = data[key]
        }
    }
      cb(card)
  }, 2 * 1000);
}

function callback(boardDetails) {
    console.log(boardDetails);
  }

  
module.exports = { allCardsThatBelongToaParticularListBasedOnThelistID,  callback }
