function detailsFindOutById( data, id, cb) {
  setTimeout(() => {
    const boardDetails = data.find((board) => board.id == id);
    cb(boardDetails);
  }, 2 * 1000);
}

function callback(boardDetails) {
  console.log(boardDetails);
}

module.exports = { detailsFindOutById, callback };
